---
 title: "Season of KDE 2021 and my first time blogging"
 date: 2021-02-20T19:17:17+04:00
 draft: false
---

## Season of KDE 2021 and my first ever blog

Hi everyone! I am Rohan Asokan. I am currently doing my undergraduate studies in Computer Science in [IIITH](https://www.iiit.ac.in/), a university in India. I can program quite proficiently in C, C++, Javascript, Python and have some knowing some basic Q#, R, FORTRAN, QBasic (I don't think even primitve coders know about this anymore). I am interested in AI/ML (obviously, cuz that seems to be trend anyways) and any tech that seems really simple but is infact as good as it gets, out of which my favourite is Ray Tracing and Ray Marching - I do have some projects on this, do checkout my github.

I am a fan of camelCase and PascalCase, so ig that says a lot about me. ;)

I am also a decent full-stack developer and am well versed with some common javascript and python frameworks for full stack development - Flask, Django, Express.js, Node.js, React.js, Vue.js, Svelte.js..(Guess I'll just stop there out of modesty).

I am also quite a bit of a gamer myself - ping me if you wanna play a few matches of Valorant or Rocket League even.

I am quite a curious soul, looking for all kinds of stuff to get myself into. And by came SoK, and I just couldn't keep myself from getting my feet wet and that's how the story begins. One project seemed to impress me quite a lot - [Kalk](https://invent.kde.org/plasma-mobile/kalk), a calculator built over the Kirigami Framework and that is exactly what I am working on. But, what happened next is the story for another day and until then have a great time.

### Some Socials:

[LinkedIn](https://www.linkedin.com/in/rohan-asokan/)

[Twitter](https://twitter.com/agentAsskon) - Not very active on it though

[Github](https://github.com/ArenaGrenade) - I am developer afterall, so this counts as a social ;)

[Gitlab](https://invent.kde.org/arenagrenade)

[Discord]() - arena_grenade#7429
